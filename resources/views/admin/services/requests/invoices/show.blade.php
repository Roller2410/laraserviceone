@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="">
            <h2 class="">Invoice For Service Request#{{$request->id}}</h2>
        </div>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif



        <a class="btn btn-primary" href="{{url()->previous() == url()->current() ? '/admin/service-requests/'.$request->id :url()->previous()}}">Back</a>
    </div>
@endsection
