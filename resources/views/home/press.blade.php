@extends('layouts.app')

@section('content')
    <div class="container py-4">
        <div class="card">
            <div class="card-header">
                Pressroom
            </div>
            <div class="card-body">
                <p class="card-text">Serviced By ONE LLC is a platform where individuals can hire professionals to complete a variety of services such as construction, catering, transporation, repairs, painting and more. We strive to have your first experience as good as the last for any service you hire us for. If your writing a story on us contact moet@servicedbyone.com</p>
            </div>
        </div>
        <br />
        <div class="card">
            <div class="card-header">
                Latest News
            </div>
            <div class="card-body">
                <p class="card-text"></p>
            </div>
        </div>
        <br />
        <div class="card">
            <div class="card-header">
                Latest Press Releases
            </div>
            <div class="card-body">
                <p class="card-text"></p>
            </div>
        </div>
    </div>
@endsection
