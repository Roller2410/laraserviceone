<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreAndUpdateServiceRequestInvoiceRequest;
use App\Models\ServiceRequest;
use App\Models\ServiceRequestInvoice;
use App\Models\ServiceRequestInvoiceDetail;
use App\Models\Seo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceRequestInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ServiceRequestInvoice::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ServiceRequest $serviceRequest)
    {
        return view('admin.services.requests.invoices.create', ['request' => $serviceRequest]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Models\ServiceRequest $serviceRequest
     * @param  \App\Http\Requests\Admin\StoreAndUpdateServiceRequestInvoiceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAndUpdateServiceRequestInvoiceRequest $request, ServiceRequest $serviceRequest)
    {
        $validated = $request->validated();
        $invoice = ServiceRequestInvoice::firstOrCreate([
            'request_id' => $serviceRequest->id
        ]);
        for($i = 0; $i < count($validated['detail']); $i++)
        {
            $invoiceDetail = ServiceRequestInvoiceDetail::create([
                'invoice_id' => (int) $invoice->id,
                'detail' => $validated['detail'][$i],
                'cost' => $validated['cost'][$i],
            ]);
        }

        if(isset($validated['mail']) && $validated['mail'])
            $serviceRequest->user->notify(new \App\Notifications\ServiceRequestInvoice($serviceRequest));
        return redirect('/admin/service-requests/'.$serviceRequest->id)->with('status', 'Invoice is created successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceRequestInvoice  $serviceRequestInvoice
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceRequestInvoice $serviceRequestInvoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServiceRequestInvoice  $serviceRequestInvoice
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceRequestInvoice $serviceRequestInvoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\StoreAndUpdateServiceRequestInvoiceRequest  $request
     * @param \App\Models\ServiceRequest $serviceRequest
     * @param  \App\Models\ServiceRequestInvoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAndUpdateServiceRequestInvoiceRequest $request, ServiceRequest $serviceRequest, ServiceRequestInvoice $invoice)
    {
        $validated = $request->validated();
        $invoice->details()->delete();
        for($i = 0; $i < count($validated['detail']); $i++)
        {
            $invoiceDetail = ServiceRequestInvoiceDetail::create([
                'invoice_id' => (int) $invoice->id,
                'detail' => $validated['detail'][$i],
                'cost' => $validated['cost'][$i],
                'quantity' => ($validated['quantity'][$i]) ? $validated['quantity'][$i] : null
            ]);
        }

        if(isset($validated['mail']) && $validated['mail'])
            $serviceRequest->user->notify(new \App\Notifications\ServiceRequestInvoice($serviceRequest));
        return redirect('/admin/service-requests/'.$serviceRequest->id)->with('status', 'Invoice is updated successfully!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServiceRequestInvoice  $serviceRequestInvoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceRequestInvoice $serviceRequestInvoice)
    {
        //
    }
}
